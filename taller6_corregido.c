#include <stdio.h>
#include <stdlib.h>

char *mesg[2] = {"Par", "impar"} ;




typedef struct estTDA{
        int c;
        int *arr;
        int cant_arr;
        char *mensaje;
}est;

void mostrar_est(est *a, int tamano);
void mostrar_int(int *a, int tamano);
int *mas(int cant);


int *mas(int cant){
        int *arr=(int *)malloc(cant * sizeof(int));

        int i = 0;

        for(i = 0; i < cant; i++){
                arr[i]  = i *10;
        }

        return arr;



}

void mostrar_est(est *a, int tamano){
        int i = 0;
        for(i = 0; i < tamano; i++){
                printf("\nEst c: %d, mensaje: %s\n", (a+i)->c, (a+i)->mensaje);
                mostrar_int((a+i)->arr,10);
                printf("\n");
        }

}

void mostrar_int(int *a , int tamano){
        int i = 0;
        char *z = (char *)a;
        for(i = 0; i <tamano; i++){
                printf("%d\n", *(z+(i*sizeof(int))));
        }
}

void fn1(int cant, int mul, int **arr){

        *arr = malloc(sizeof(int *)*cant);

        if(arr == NULL){
                return;
        }
        
        int i = 0;
        for(i = 0; i < cant; i++){
                *(*(arr)+i) = mul*i;
        }
}

void fn2(int cant, est **res){
        est *data = malloc(sizeof(est *)*cant);

        if(data == NULL){
                return;
        }


        int i = 0;
        for(i = 0; i <= cant ; i++){
                (data+i)->c = i;
                fn1(i,3, &data[i].arr);
                data[i].cant_arr = i;
                (data+i)->mensaje = mesg[ i % 2 ];

        }

        *res = data;

}

int main(int argc, char **argv){

        if(argc < 2){
                printf("Uso: ./progra <numero de elemenetos>\n");
                exit(EXIT_FAILURE);
        }

        int cant = atoi(argv[1]);
        if(cant < 10){
                printf("Arguento debe ser al menos 10\n");
                exit(EXIT_FAILURE);
        }


        int *arr;
        fn1(cant, 2, &arr);

        est *res = NULL;
        fn2(cant, &res);
        printf("ARREGLO 1\n");

        
        mostrar_int(arr, cant);
        printf("ARREGLO 2\n");
        int *arr2 = mas(cant);
        mostrar_est(res, cant);
        mostrar_int(arr2,cant);

        free(arr);
        arr = NULL;

        exit(EXIT_SUCCESS);
}